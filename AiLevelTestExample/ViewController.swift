//
//  ViewController.swift
//  AiLevelTestExample
//
//  Created by Jun-kyu Jeon on 2021/01/06.
//

import UIKit

import AiLevelTestKit

class ViewController: UIViewController {
    private let _tableView = UITableView()
    
    private var _tableData = [ALTExamData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _tableView.translatesAutoresizingMaskIntoConstraints = false
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.register(UITableViewCell.self, forCellReuseIdentifier: "defaultCell")
        self.view.addSubview(_tableView)
        
        _tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        _tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        _tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        _tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        // ** 레벨테스트 활성화. AiLevelTestKit.shared.activate
        // groupCode : 할당받은 그룹코드
        // email : 이메일 
        // themeColour : 테마 색상
        // completion(code, errMessage) : 활성화 작업 완료 후 callback
        //      code : (ALTResponseCode)    Succeed : 활성화 성공
        //                                  Failed : 활성화 실패
        //                                  Unknown : 활성화 실패 (알 수 없음)
        //      errMessage : (String?) :  에러메세지
        
        AiLevelTestKit.shared.activate(groupCode: "allinone51379", email: "1", themeColour: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)) { [weak self] (code, errMessage) in
            guard code == .Succeed else {
                // 초기화 실패시 실패 사유를 alert으로 보여준다
                
                let alertController = UIAlertController(title: errMessage, message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
                self?.present(alertController, animated: true, completion: nil)
                
                return
            }
            // 초기화 성공시 테이블 뷰를 갱신한다.
            self?.reloadTableData()
        }
    }

    private func reloadTableData() {
        // ** 레벨테스트 시험 목록 가져오기. AiLevelTestKit.shared.activate
        // completion(code, errMessage) : 활성화 작업 완료 후 callback
        //      code : (ALTResponseCode)    Succeed : 리스트 가져오기 성공
        //                                  Failed : 리스트 가져오기 실패
        //                                  Unknown : 리스트 가져오기 실패 (알 수 없음)
        //      errMessage : (String?) :  에러메세지
        //      examList : ([ALTExamData]?) :  시험 데이터 목록
        AiLevelTestKit.shared.getExamList {[weak self] (code, errMessage, examList) in
            self?._tableData.removeAll()
            
            if code == .Succeed, examList != nil {
                // 리스트 가져오기 성공시 테이블 뷰 데이터 업데이트
                self?._tableData.append(contentsOf: examList!)
            }
            
            self?._tableView.reloadData()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < _tableData.count else { return }
        
        let data = _tableData[indexPath.row]
        
        // 테스트 시작
        AiLevelTestKit.shared.startTest(from: self, with: data)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell") else { return UITableViewCell() }
        
        cell.selectionStyle = .none
        
        let data = _tableData[indexPath.row]
        cell.textLabel?.text = data.title
        
        return cell
    }
}

