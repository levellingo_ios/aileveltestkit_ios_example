// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target arm64-apple-ios12.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name AiLevelTestKit
import AVFoundation
import Accelerate
import CommonCrypto
import CoreGraphics
import Foundation
import NotificationCenter
import Photos
import QuartzCore
import SQLite3
import Speech
import Swift
import UIKit
import WebKit
@objc @_hasMissingDesignatedInitializers public class ALTBaseData : ObjectiveC.NSObject {
  @objc required public init?(coder aDecoder: Foundation.NSCoder)
  required public init(_ data: AiLevelTestKit.ALTBaseData)
  @objc override dynamic public init()
  @objc deinit
}
extension ALTBaseData : Foundation.NSCopying, Foundation.NSCoding {
  @objc dynamic public func copy(with zone: ObjectiveC.NSZone? = nil) -> Any
  @objc dynamic public func encode(with aCoder: Foundation.NSCoder)
}
public enum ALTResponseCode : Swift.Int {
  case Succeed
  case Failed
  case Unknown
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_hasMissingDesignatedInitializers @IBDesignable public class WaveView : UIKit.UIView {
  @objc @IBInspectable public var color1: UIKit.UIColor
  @objc @IBInspectable public var color2: UIKit.UIColor
  @objc @IBInspectable public var color3: UIKit.UIColor
  @objc @IBInspectable public var color4: UIKit.UIColor
  @objc @IBInspectable public var color5: UIKit.UIColor
  @objc @IBInspectable public var value: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc dynamic public init()
  @objc override dynamic public func draw(_ rect: CoreGraphics.CGRect)
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
}
extension UINavigationController {
  @objc override dynamic open var childForStatusBarStyle: UIKit.UIViewController? {
    @objc get
  }
}
@objc @_hasMissingDesignatedInitializers @IBDesignable public class SimpleWaveView : UIKit.UIView {
  @objc @IBInspectable public var color: UIKit.UIColor
  @objc @IBInspectable public var lineWidth: CoreGraphics.CGFloat
  @objc @IBInspectable public var value: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc dynamic public init()
  @objc override dynamic public func draw(_ rect: CoreGraphics.CGRect)
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
}
public struct ColourKit {
  public struct Code {
    public static var Hex000000: UIKit.UIColor
    public static var Hex121212: UIKit.UIColor
    public static var Hex222222: UIKit.UIColor
    public static var Hex27272C: UIKit.UIColor
    public static var Hex3E3A39: UIKit.UIColor
    public static var Hex555555: UIKit.UIColor
    public static var Hex888888: UIKit.UIColor
    public static var Hex8A8A8A: UIKit.UIColor
    public static var HexAAAAAA: UIKit.UIColor
    public static var HexA0A0A0: UIKit.UIColor
    public static var HexB3B3B3: UIKit.UIColor
    public static var HexC0C0C0: UIKit.UIColor
    public static var HexCCCCCC: UIKit.UIColor
    public static var HexCDDBE0: UIKit.UIColor
    public static var HexD0DBDF: UIKit.UIColor
    public static var HexD0D0D0: UIKit.UIColor
    public static var HexEEEEEE: UIKit.UIColor
    public static var HexF0F0F0: UIKit.UIColor
    public static var HexFFFFFF: UIKit.UIColor
    public static var Hex727B88: UIKit.UIColor
    public struct Static {
      public static var Hex00C9B7: UIKit.UIColor
      public static var Hex222222: UIKit.UIColor
      public static var Hex3296D7: UIKit.UIColor
      public static var Hex2D78CD: UIKit.UIColor
      public static var HexF764AB: UIKit.UIColor
      public static var HexE1B496: UIKit.UIColor
      public static var Hex69A3ED: UIKit.UIColor
      public static var Hex5CC4BA: UIKit.UIColor
      public static var Hex5DD3BC: UIKit.UIColor
      public static var Hex7F58D2: UIKit.UIColor
      public static var Hex2898C5: UIKit.UIColor
      public static var HexA9D69C: UIKit.UIColor
      public static var HexC343A7: UIKit.UIColor
      public static var HexF44F69: UIKit.UIColor
      public static var HexFB654F: UIKit.UIColor
      public static var HexFDBC58: UIKit.UIColor
      public static var HexFA4175: UIKit.UIColor
      public static var HexFFFF91: UIKit.UIColor
    }
  }
}
@_inheritsConvenienceInitializers @objc public class BEMCheckBoxGroup : ObjectiveC.NSObject {
  @objc public var checkBoxes: Foundation.NSHashTable<AiLevelTestKit.BEMCheckBox> {
    get
  }
  @objc public var selectedCheckBox: AiLevelTestKit.BEMCheckBox? {
    @objc get
    @objc set(newValue)
  }
  @objc public var mustHaveSelection: Swift.Bool {
    @objc get
    @objc set(value)
  }
  @objc override dynamic public init()
  @objc public convenience init(checkBoxes: [AiLevelTestKit.BEMCheckBox])
  @objc public func contains(_ checkBox: AiLevelTestKit.BEMCheckBox) -> Swift.Bool
  @objc public func addCheckBoxToGroup(_ checkBox: AiLevelTestKit.BEMCheckBox)
  @objc public func removeCheckBoxFromGroup(_ checkBox: AiLevelTestKit.BEMCheckBox)
  @objc deinit
}
@objc final public class BEMAnimationManager : ObjectiveC.NSObject {
  @objc final public var animationDuration: Swift.Double
  @objc public init(animationDuration: CoreFoundation.CFTimeInterval)
  @objc final public func strokeAnimationReverse(_ reverse: Swift.Bool) -> QuartzCore.CABasicAnimation
  @objc final public func opacityAnimationReverse(_ reverse: Swift.Bool) -> QuartzCore.CABasicAnimation
  @objc final public func morphAnimation(from fromPath: UIKit.UIBezierPath?, to toPath: UIKit.UIBezierPath?) -> QuartzCore.CABasicAnimation
  @objc final public func fillAnimation(withBounces bounces: Swift.Int, amplitude: CoreGraphics.CGFloat, reverse: Swift.Bool) -> QuartzCore.CAKeyframeAnimation
  @objc deinit
  @objc override dynamic public init()
}
public class BowlSenoid {
  public init(center: Swift.Double)
  public func f(_ x: Swift.Double) -> Swift.Double
  public func f(_ x: CoreGraphics.CGFloat) -> CoreGraphics.CGFloat
  public func g(_ x: CoreGraphics.CGFloat) -> CoreGraphics.CGFloat
  @objc deinit
}
@objc public protocol BEMCheckBoxDelegate : ObjectiveC.NSObjectProtocol {
  @objc optional func didTap(_ checkBox: AiLevelTestKit.BEMCheckBox)
  @objc optional func animationDidStop(for checkBox: AiLevelTestKit.BEMCheckBox)
}
@_inheritsConvenienceInitializers @IBDesignable @objc public class BEMCheckBox : UIKit.UIControl, QuartzCore.CAAnimationDelegate {
  @objc(BEMBoxType) public enum BoxType : Swift.Int {
    case circle
    case square
    public typealias RawValue = Swift.Int
    public init?(rawValue: Swift.Int)
    public var rawValue: Swift.Int {
      get
    }
  }
  @objc(BEMAnimationType) public enum AnimationType : Swift.Int {
    case stroke
    case fill
    case bounce
    case flat
    case oneStroke
    case fade
    public typealias RawValue = Swift.Int
    public init?(rawValue: Swift.Int)
    public var rawValue: Swift.Int {
      get
    }
  }
  @objc @IBOutlet weak public var delegate: AiLevelTestKit.BEMCheckBoxDelegate?
  @objc @IBInspectable public var on: Swift.Bool {
    @objc get
    @objc set(newValue)
  }
  @objc public var boxType: AiLevelTestKit.BEMCheckBox.BoxType {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable public var lineWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable public var cornerRadius: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable public var animationDuration: Swift.Double {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable public var hideBox: Swift.Bool
  @objc @IBInspectable public var onTintColor: UIKit.UIColor?
  @objc @IBInspectable public var onFillColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable public var offFillColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable public var onCheckColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc public var group: AiLevelTestKit.BEMCheckBoxGroup?
  @objc public var onAnimationType: AiLevelTestKit.BEMCheckBox.AnimationType
  @objc public var offAnimationType: AiLevelTestKit.BEMCheckBox.AnimationType
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc override dynamic public var frame: CoreGraphics.CGRect {
    @objc get
    @objc set(value)
  }
  @objc override dynamic public func layoutSubviews()
  @objc override dynamic public var intrinsicContentSize: CoreGraphics.CGSize {
    @objc get
  }
  @objc public func setOn(_ on: Swift.Bool, animated: Swift.Bool = false)
  @objc override dynamic public func point(inside point: CoreGraphics.CGPoint, with event: UIKit.UIEvent?) -> Swift.Bool
  @objc public func animationDidStop(_ anim: QuartzCore.CAAnimation, finished flag: Swift.Bool)
  @objc deinit
}
@objc @_hasMissingDesignatedInitializers @IBDesignable public class BarsWaveView : UIKit.UIView {
  @objc @IBInspectable public var color: UIKit.UIColor
  @objc @IBInspectable public var lineWidth: CoreGraphics.CGFloat
  @objc @IBInspectable public var amplitude: CoreGraphics.CGFloat
  @objc @IBInspectable public var value: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc dynamic public init()
  @objc override dynamic public func draw(_ rect: CoreGraphics.CGRect)
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class ALTExamData : AiLevelTestKit.ALTBaseData {
  public var identifier: Swift.String? {
    get
  }
  public var title: Swift.String? {
    get
  }
  public var setupSrl: Swift.Int? {
    get
  }
  public var examSrl: Swift.Int? {
    get
  }
  public var groupSrl: Swift.Int? {
    get
  }
  public var limitTime: Swift.Int? {
    get
  }
  public var isPrivacyActivated: Swift.Bool {
    get
  }
  public var isCouponActivated: Swift.Bool {
    get
  }
  public var isTutorialActivated: Swift.Bool {
    get
  }
  public var isMicTestActivated: Swift.Bool {
    get
  }
  public var isMyPageView1: Swift.Bool {
    get
  }
  public var isMyPageView2: Swift.Bool {
    get
  }
  public var isMyPageView3: Swift.Bool {
    get
  }
  public var isMyPageView4: Swift.Bool {
    get
  }
  public var examSetup1: Swift.Int? {
    get
  }
  public var examSetup2: Swift.Int? {
    get
  }
  public var examSetup3: Swift.Int? {
    get
  }
  public var myPageSetup1: Swift.Int? {
    get
  }
  public var theme: Swift.String? {
    get
  }
  public var userLanguage: Swift.String? {
    get
  }
  public var testLanguage: Swift.String? {
    get
  }
  @objc deinit
  @objc required public init?(coder aDecoder: Foundation.NSCoder)
  required public init(_ data: AiLevelTestKit.ALTBaseData)
}
@_hasMissingDesignatedInitializers public class AiLevelTestKit {
  public static var shared: AiLevelTestKit.AiLevelTestKit
  public var themeColour: UIKit.UIColor
  public var groupCode: Swift.String? {
    get
  }
  public var email: Swift.String? {
    get
  }
  public var userGroupTitle: Swift.String? {
    get
  }
  public func initialize()
  public func activate(groupCode: Swift.String? = nil, email: Swift.String? = nil, themeColour: UIKit.UIColor = #colorLiteral(red: 0.9294117647, green: 0.07843137255, blue: 0.3568627451, alpha: 1), completion: @escaping ((AiLevelTestKit.ALTResponseCode, Swift.String?) -> Swift.Void))
  public func getExamList(_ completion: @escaping ((AiLevelTestKit.ALTResponseCode, Swift.String?, [AiLevelTestKit.ALTExamData]?) -> Swift.Void))
  public func startTest(from viewController: UIKit.UIViewController, with exam: AiLevelTestKit.ALTExamData)
  @objc deinit
}
